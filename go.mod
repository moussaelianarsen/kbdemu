module kbdemu

go 1.15

require (
	gitea.arsenm.dev/Arsen6331/scpt v0.0.0-20210405182847-836149ffc24f
	github.com/gen2brain/beeep v0.0.0-20200526185328-e9c15c258e28
	github.com/gen2brain/dlgs v0.0.0-20210222160047-2f436553172f
	github.com/go-vgo/robotgo v0.92.1
	github.com/rs/zerolog v1.20.0
	github.com/skratchdot/open-golang v0.0.0-20200116055534-eef842397966
	github.com/spf13/pflag v1.0.5
)
