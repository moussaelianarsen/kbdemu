/*
	Copyright 2021 Arsen Musayelyan

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

package main

import (
	"errors"
	"fmt"
	"github.com/gen2brain/dlgs"
	"github.com/skratchdot/open-golang/open"
	"time"
)

// Sleep for the given duration
func Sleep(args map[string]interface{}) (interface{}, error) {
	// Get duration and assert as string
	durationStr, ok := args[""].(string)
	if !ok {
		return nil, errors.New("sleep requires an unnamed string argument with duration")
	}
	// Parse duration string
	duration, err := time.ParseDuration(durationStr)
	if err != nil {
		return nil, err
	}
	// Sleep for specified duration
	time.Sleep(duration)
	return nil, nil
}

// Default function to display a dialog
func displayDialog(args map[string]interface{}) (interface{}, error) {
	// Get title
	title, ok := args["title"]
	if !ok {
		// If title not given, set to empty string
		title = ""
	}
	// Get unnamed argument as text
	text, ok := args[""]
	if !ok {
		return nil, errors.New("display-dialog requires an unnamed string argument with dialog text")
	}
	// Display correct dialog based on given type
	switch args["type"] {
	case "yesno":
		// Display yes or no dialog, returning bool based on user input
		return dlgs.Question(fmt.Sprint(title), fmt.Sprint(text), true)
	case "info":
		// Display info dialog, returning bool based on success
		return dlgs.Info(fmt.Sprint(title), fmt.Sprint(text))
	case "warning":
		// Display error dialog, returning bool based on success
		return dlgs.Warning(fmt.Sprint(title), fmt.Sprint(text))
	case "error":
		// Display error dialog, returning bool based on success
		return dlgs.Error(fmt.Sprint(title), fmt.Sprint(text))
	case "entry":
		// Check if default text given
		defaultText, ok := args["default"]
		if !ok {
			// Set to empty if not given
			defaultText = ""
		}
		// Display entry dialog
		input, _, err := dlgs.Entry(fmt.Sprint(title), fmt.Sprint(text), fmt.Sprint(defaultText))
		// Return user input
		return input, err
	default:
		// If type unknown, display info dialog, returning bool based on success
		return dlgs.Info(fmt.Sprint(title), fmt.Sprint(text))
	}
}

func userChoice(args map[string]interface{}) (interface{}, error) {
	// Get title
	title, ok := args["title"]
	if !ok {
		// If title not given, set to empty string
		title = ""
	}
	// Get title
	items, ok := args["items"].([]interface{})
	if !ok {
		return nil, errors.New("user-choice requires an array argument named items")
	}
	// Get unnamed argument as text
	text, ok := args[""]
	if !ok {
		return nil, errors.New("user-choice requires an unnamed argument with dialog text")
	}
	var strItems []string
	for _, item := range items {
		strItems = append(strItems, fmt.Sprint(item))
	}
	choice, _, err := dlgs.List(fmt.Sprint(title), fmt.Sprint(text), strItems)
	return choice, err
}

func openLocation(args map[string]interface{}) (interface{}, error) {
	url, ok := args[""].(string)
	if !ok {
		return nil, errors.New("open-location requires an unnamed string argument with a url")
	}
	err := open.Start(url)
	if err != nil {
		return nil, err
	}
	return nil, nil
}