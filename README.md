# KbdEmu

---

### What is KbdEmu?

KbdEmu is a simple cross-platform interpreter using one of my projects ([scpt](https://gitea.arsenm.dev/Arsen6331/scpt))
created for automation of various actions and events. It is called KbdEmu as its original purpose was a keyboard emulator.

### Installing

To install, simply clone or download this repo, and then run:
```shell
go build
sudo install -Dm755 kbdemu /usr/bin/kbdemu
```
