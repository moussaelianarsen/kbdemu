package main

import (
	"errors"
	"fmt"
	"github.com/gen2brain/beeep"
	"time"
)

func sendNotif(args map[string]interface{}) (interface{}, error) {
	body, ok := args[""].(string)
	if !ok {
		return nil, errors.New("send-notification requires an unnamed string argument with notification body")
	}
	title, ok := args["title"].(string)
	if !ok {
		title = ""
	}
	icon, ok := args["icon"].(string)
	if !ok {
		icon = ""
	}
	err := beeep.Alert(title, body, icon)
	if err != nil {
		return nil, err
	}
	return nil, nil
}

func beep(args map[string]interface{}) (interface{}, error) {
	// Get duration and assert as string
	durationStr, ok := args[""].(string)
	if !ok {
		durationStr = fmt.Sprint(beeep.DefaultDuration, "ms")
	}
	// Parse duration string
	duration, err := time.ParseDuration(durationStr)
	if err != nil {
		return nil, err
	}
	err = beeep.Beep(beeep.DefaultFreq, int(duration.Milliseconds()))
	if err != nil {
		return nil, err
	}
	return nil, nil
}