/*
	Copyright 2021 Arsen Musayelyan

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

package main

import (
	"errors"
	"github.com/go-vgo/robotgo"
)

// Perform an action on a key
func Key(args map[string]interface{}) (interface{}, error) {
	// Get unnamed argument and assert as string
	key, ok := args[""].(string)
	if !ok {
		return nil, errors.New("keystroke requires an unnamed string argument with a key")
	}
	// Get action argument and assert as string
	action, ok := args["action"].(string)
	if !ok {
		// If assertion fails or action not given, set to tap
		action = "tap"
	}
	// Perform action based on given parameters
	switch action {
	case "hold":
		// Hold key
		robotgo.KeyToggle(key, "down")
	case "release":
		// Release key
		robotgo.KeyToggle(key, "up")
	case "tap":
		// Tap key once
		robotgo.KeyTap(key)
	}
	return nil, nil
}

// Type a string using the keyboard
func Type(args map[string]interface{}) (interface{}, error) {
	// Get unnamed argument and assert as string
	str, ok := args[""].(string)
	if !ok {
		return nil, errors.New("type requires an unnamed argument with the string to type")
	}
	// Type string
	robotgo.TypeStr(str)
	return nil, nil
}
