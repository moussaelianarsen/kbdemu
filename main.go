/*
	Copyright 2021 Arsen Musayelyan

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

package main

import (
	"gitea.arsenm.dev/Arsen6331/scpt"
	flag "github.com/spf13/pflag"
	"os"
	"runtime"
)

func main() {
	// Create new flag for location of kbdemu file
	scptFile := flag.StringP("file", "f", "kbdemu.scpt", "Scpt file to use for kbdemu")
	// Parse flags
	flag.Parse()

	// Open specified scpt file
	file, err := os.Open(*scptFile)
	if err != nil {
		Log.Fatal().Err(err).Msg("Error opening specified file")
	}
	defer file.Close()

	// Parse opened scpt file
	ast, err := scpt.Parse(file)
	if err != nil {
		Log.Fatal().Err(err).Msg("Error parsing scpt file")
	}

	// Create new nil interface slice
	var args []interface{}
	// For each argument
	for _, arg := range flag.Args() {
		// Append to interface slice
		args = append(args, arg)
	}

	// Add variables to scpt runtime
	scpt.AddVars(map[string]interface{}{
		"GOOS": runtime.GOOS,
		"GOARCH": runtime.GOARCH,
		"arguments": args,
	})
	// Add functions to scpt runtime
	scpt.AddFuncs(scpt.FuncMap{
		"numcpu": GetCPUNum,
		"sleep": Sleep,
		"display-dialog": displayDialog,
		"send-notification": sendNotif,
		"beep": beep,
		"click": Click,
		"scroll": Scroll,
		"move-mouse": MoveMouse,
		"keystroke": Key,
		"type": Type,
		"mouse-position": MousePos,
		"pixel-color": PixelColor,
		"log": scptLog,
		"user-choice": userChoice,
		"open-location": openLocation,
	})

	// Execute scpt Abstract Syntax Tree
	err = ast.Execute()
	if err != nil {
		Log.Fatal().Msg(err.Error())
	}
}
