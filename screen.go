/*
	Copyright 2021 Arsen Musayelyan

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

package main

import (
	"errors"
	"github.com/go-vgo/robotgo"
)

// Get hex color of a pixel at a specific location on the screen
func PixelColor(args map[string]interface{}) (interface{}, error) {
	// Get coordinates array and assert as slice
	coords, ok := args[""].([]interface{})
	if !ok {
		return nil, errors.New("pixel-color requires an unnamed array argument with number coordinates")
	}
	// Get x coordinate from array and assert as float64
	xCoord, ok := coords[0].(float64)
	if !ok {
		return nil, errors.New("first item in array must be an x coordinate of type number")
	}
	// Get y coordinate from array and assert as float64
	yCoord, ok := coords[1].(float64)
	if !ok {
		return nil, errors.New("second item in array must be a y coordinate of type number")
	}
	// Get and return color string of pixel at specified location
	return robotgo.GetPixelColor(int(xCoord), int(yCoord)), nil
}

// Get current position of the mouse
func MousePos(_ map[string]interface{}) (interface{}, error) {
	// Get mouse position
	xCoord, yCoord := robotgo.GetMousePos()
	// Return interface slice with coordinates
	return []interface{}{float64(xCoord), float64(yCoord)}, nil
}
