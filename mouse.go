/*
	Copyright 2021 Arsen Musayelyan

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

package main

import (
	"errors"
	"github.com/go-vgo/robotgo"
)

// Click a specified mouse button
func Click(args map[string]interface{}) (interface{}, error) {
	// Get button to click and assert as string
	button, ok := args[""].(string)
	if !ok {
		return nil, errors.New("click requres an unnamed string argument")
	}
	// Click button
	robotgo.MouseClick(button)
	return nil, nil
}

// Scroll a specified distance in a specified direction
func Scroll(args map[string]interface{}) (interface{}, error) {
	// Get scroll distance and assert as float64
	scrollDistance, ok := args[""].(float64)
	if !ok {
		return nil, errors.New("scroll requires an unnamed number argument")
	}
	// Get scroll direction and assert as string
	scrollDirection, ok := args["direction"].(string)
	if !ok {
		return nil, errors.New("scroll requires a string argument named direction")
	}
	// Scroll mouse with specified parameters
	robotgo.ScrollMouse(int(scrollDistance), scrollDirection)
	return nil, nil
}

// Move the mouse to a specified location
func MoveMouse(args map[string]interface{}) (interface{}, error) {
	// Get coordinates array and assert as slice
	coords, ok := args[""].([]interface{})
	if !ok {
		return nil, errors.New("move-mouse requires an unnamed array argument with number coordinates")
	}
	// Get x coordinate from array and assert as float64
	xCoord, ok := coords[0].(float64)
	if !ok {
		return nil, errors.New("first item in array must be an x coordinate of type number")
	}
	// Get y coordinate from array and assert as float64
	yCoord, ok := coords[1].(float64)
	if !ok {
		return nil, errors.New("second item in array must be a y coordinate of type number")
	}
	// Move mouse to specified coordinates
	robotgo.MoveMouse(int(xCoord), int(yCoord))
	return nil, nil
}
