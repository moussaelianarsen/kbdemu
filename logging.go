/*
	Copyright 2021 Arsen Musayelyan

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
 */

package main

import (
	"errors"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"os"
)

// Set global logger to ConsoleWriter
var Log = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

// Starlark builtin that logs at the optionally provided level
func scptLog(args map[string]interface{}) (interface{}, error) {
	// Get message and assert as string
	message, ok := args[""].(string)
	if !ok {
		return nil, errors.New("log requires an unnamed string argument with the message")
	}
	// Get log level
	level, ok := args["level"]
	if !ok {
		// If log level not given, set to info
		level = "info"
	}
	var leveledLogger *zerolog.Event
	// Determine and set desired logger
	switch level {
	case "info":
		leveledLogger = Log.Info()
	case "debug":
		leveledLogger = Log.Debug()
	case "warn":
		leveledLogger = Log.Warn()
	case "fatal":
		leveledLogger = Log.Fatal()
	default:
		leveledLogger = Log.Info()
	}
	// Send log event
	leveledLogger.Msg(message)
	// Return none if successful
	return nil, nil
}
