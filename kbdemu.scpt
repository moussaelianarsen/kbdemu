set testKey to "x"
keystroke $testKey with action "hold"
sleep "1s"
keystroke $testKey with action "release"
type "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz"
scroll 5 with direction "up"
click "right"
move-mouse [0, 0]
send-notification "Test"
beep
open-location "https://www.arsenm.dev/"
set showDetails to (display-dialog "Show details?" with type "yesno")
define goroutineTest {
    if $showDetails {
      display-dialog {"Color: " + (pixel-color [100, 100]) + ", Mouse: " + (str (mouse-position))} with title "Details"
    }
}
go goroutineTest
print {"\n" + (user-choice "test" with items ["Hello", "World", 3.1415926535, $GOOS, $GOARCH, true, false, (numcpu)])}
print $arguments
log "Complete!"